/**
 * 
 */
package com.hbajar.util;

import java.lang.reflect.Field;

import org.apache.log4j.Logger;

import com.hbajar.beans.BaseBean;
import com.hbajar.beans.PostBean;
import com.hbajar.beans.ResultParamsBean;
import com.hbajar.beans.UserBean;

/**
 * @author SBhattarai
 *
 */
public class LoggerUtil {
	private static Logger LOG = Logger.getLogger(LoggerUtil.class);

	public static void log(PostBean obj) throws Exception {
		logBean(obj);
	}

	public static void log(BaseBean obj) throws Exception {
		logBean(obj);
	}

	public static void log(ResultParamsBean obj) throws Exception {
		logBean(obj);
	}

	public static void log(UserBean obj) throws Exception {
		logBean(obj);
	}

	private static void logBean(Object obj) throws Exception {
		StringBuilder info = new StringBuilder();
		for (Field field : obj.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			String name = field.getName();
			Object value = field.get(obj);
			info.append(name + ":" + value + " ");
		}
		LOG.info(info);
	}

}
