/**
 *
 */
package com.hbajar.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.hbajar.beans.ResultParamsBean;
import com.hbajar.entities.Post;

/**
 * @author SBhattarai
 *
 */
public class DaoUtil {

	private static final byte ACTIVE = 1;
	private static final int UNLIM_SIZE = 1000000;

	static Calendar cal = null;

	@SuppressWarnings("unchecked")
	public static List<Post> getUserLocationSpecificPosts(final ResultParamsBean resultParams, final Session session, final int resultSize,
			final int resultDays) {

		List<Post> posts = new ArrayList<Post>();
		cal = Calendar.getInstance(Locale.US);
		cal.add(Calendar.DATE, -resultDays);

		// All Matching
		posts.addAll(getPostsQry1(resultParams, session, resultSize, resultParams.getRowNum()).list());

		// Matching state and Country
		if (posts.size() < resultSize) {
			int firstRowNum = calculateFirstResult(resultParams, posts.size(), resultSize, session);
			posts.addAll(getPostsQry2(resultParams, session, resultSize, firstRowNum, posts.size()).list());
		}

		// Matching Country
		if (posts.size() < resultSize) {
			int firstRowNum = calculateFirstResult(resultParams, posts.size(), resultSize, session);
			posts.addAll(getPostsQry3(resultParams, session, resultSize, firstRowNum, posts.size()).list());
		}

		if (posts.size() < resultSize) {
			int firstRowNum = calculateFirstResult(resultParams, posts.size(), resultSize, session);
			posts.addAll(getPostsQry4(resultParams, session, resultSize, firstRowNum, posts.size()).list());
		}

		return posts;
	}

	private static Criteria getPostsQry1(ResultParamsBean resultParams, final Session session, final int resultSize, int firstResult) {
		return session.createCriteria(Post.class).add(Restrictions.eq("postCity", resultParams.getPostCity()))
				.add(Restrictions.eq("postState", resultParams.getPostState())).add(Restrictions.eq("postCountry", resultParams.getPostCountry()))
				.add(Restrictions.eq("active", ACTIVE)).add(Restrictions.ge("postTime", cal.getTime())).setFirstResult(firstResult)
				.setMaxResults(resultSize).addOrder(Order.asc("priority")).addOrder(Order.asc("postTime"));
	}

	private static Criteria getPostsQry2(ResultParamsBean resultParams, final Session session, final int resultSize, int firstResult, int postSize) {
		return session.createCriteria(Post.class).add(Restrictions.ne("postCity", resultParams.getPostCity()))
				.add(Restrictions.eq("postState", resultParams.getPostState())).add(Restrictions.eq("postCountry", resultParams.getPostCountry()))
				.add(Restrictions.eq("active", ACTIVE)).add(Restrictions.ge("postTime", cal.getTime())).setFirstResult(firstResult)
				.setMaxResults(resultSize - postSize).addOrder(Order.asc("priority")).addOrder(Order.asc("postTime"));
	}

	private static Criteria getPostsQry3(ResultParamsBean resultParams, final Session session, final int resultSize, int firstResult, int postSize) {
		return session.createCriteria(Post.class).add(Restrictions.ne("postCity", resultParams.getPostCity()))
				.add(Restrictions.ne("postState", resultParams.getPostState())).add(Restrictions.eq("postCountry", resultParams.getPostCountry()))
				.add(Restrictions.eq("active", ACTIVE)).add(Restrictions.ge("postTime", cal.getTime())).setFirstResult(firstResult)
				.setMaxResults(resultSize - postSize).addOrder(Order.asc("priority")).addOrder(Order.asc("postTime"));
	}

	private static Criteria getPostsQry4(ResultParamsBean resultParams, final Session session, final int resultSize, int firstResult, int postSize) {
		return session.createCriteria(Post.class).add(Restrictions.ne("postCountry", resultParams.getPostCountry()))
				.add(Restrictions.eq("active", ACTIVE)).add(Restrictions.ge("postTime", cal.getTime())).setFirstResult(firstResult)
				.setMaxResults(resultSize - postSize).addOrder(Order.asc("priority")).addOrder(Order.asc("postTime"));
	}

	private static int calculateFirstResult(ResultParamsBean resultParams, int currentPostsSize, int requestedResultSize, Session session) {
		if (currentPostsSize > 0) { // list is partially populated. 18 20 11 22
			return 0;
		} else {
			// If this is not first request
			if (resultParams.getRowNum() >= requestedResultSize) {
				// if the result was first calculated by first query
				long qry1Size = (long) getPostsQry1(resultParams, session, UNLIM_SIZE, 0).setProjection(Projections.rowCount()).uniqueResult();
				long qry2Size = (long) getPostsQry2(resultParams, session, UNLIM_SIZE, 0, 0).setProjection(Projections.rowCount()).uniqueResult();
				// result was first calculated by first query and partially by
				// second query
				if (resultParams.getRowNum() - qry1Size < qry2Size) {
					return (int) (resultParams.getRowNum() - qry1Size);
				} else {
					// calculate size of second query
					long qry3Size = (long) getPostsQry3(resultParams, session, UNLIM_SIZE, 0, 0).setProjection(Projections.rowCount()).uniqueResult();
					if (resultParams.getRowNum() - (qry1Size + qry2Size) < qry3Size) {
						return (int) (resultParams.getRowNum() - (qry1Size + qry2Size));
					} else {
						long qry4Size = (long) getPostsQry4(resultParams, session, UNLIM_SIZE, 0, 0).setProjection(Projections.rowCount())
								.uniqueResult();
						if (resultParams.getRowNum() - (qry1Size + qry2Size + qry3Size) < qry4Size) {
							return (int) (resultParams.getRowNum() - (qry1Size + qry2Size + qry3Size));
						}
					}
				}
			}
		}
		return 0;

	}

	@SuppressWarnings("unchecked")
	public static List<Post> getUserLocationAndCategorySpecificPosts(final ResultParamsBean resultParams, final Session session,
			final int resultSize, final int resultDays) {
		List<Post> posts = new ArrayList<Post>();

		final Calendar cal = Calendar.getInstance(Locale.US);
		cal.add(Calendar.DATE, -resultDays);

		// All Matching
		posts = session.createCriteria(Post.class).add(Restrictions.eq("postCity", resultParams.getPostCity()))
				.add(Restrictions.eq("postState", resultParams.getPostState())).add(Restrictions.eq("postCountry", resultParams.getPostCountry()))
				.add(Restrictions.eq("active", ACTIVE)).add(Restrictions.eq("category", resultParams.getCategory()))
				.add(Restrictions.ge("postDate", cal.getTime())).setFirstResult(resultParams.getRowNum()).setMaxResults(resultSize)
				.addOrder(Order.asc("priority")).addOrder(Order.asc("postTime")).list();

		// Matching state and Country
		if (posts == null || posts.size() < resultSize) {
			posts.addAll(session.createCriteria(Post.class).add(Restrictions.eq("postState", resultParams.getPostState()))
					.add(Restrictions.eq("postCountry", resultParams.getPostCountry())).add(Restrictions.eq("active", ACTIVE))
					.add(Restrictions.eq("category", resultParams.getCategory())).add(Restrictions.ge("postDate", cal.getTime()))
					.setFirstResult(resultParams.getRowNum() + posts.size()).setMaxResults(resultSize - posts.size()).addOrder(Order.asc("priority"))
					.addOrder(Order.asc("postTime")).list());
		}

		// Matching Country
		if (posts == null || posts.size() < resultSize) {
			posts.addAll(session.createCriteria(Post.class).add(Restrictions.eq("postCountry", resultParams.getPostCountry()))
					.add(Restrictions.eq("active", ACTIVE)).add(Restrictions.eq("category", resultParams.getCategory()))
					.add(Restrictions.ge("postDate", cal.getTime())).setFirstResult(resultParams.getRowNum() + posts.size())
					.setMaxResults(resultSize - posts.size()).addOrder(Order.asc("priority")).addOrder(Order.asc("postTime")).list());
		}

		return posts;
	}

}
