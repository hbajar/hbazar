/**
 *
 */
package com.hbajar.util;

import java.util.Date;

import com.hbajar.beans.PostBean;
import com.hbajar.beans.UserBean;
import com.hbajar.entities.Post;
import com.hbajar.entities.User;

/**
 * @author SBhattarai
 *
 */
public class TransformerUtil {

	private static final byte ACTIVE = 1;

	public static User getUserEntity(final UserBean userBean) {
		User user = null;
		if (userBean != null && userBean.getEmail() != null) {
			user = new User();
			user.setEmail(userBean.getEmail());
			user.setFirstName(userBean.getFirstName());
			user.setLastName(userBean.getLastName());
			user.setLastLogin(userBean.getLastLogin());
			user.setLastUpdate(userBean.getLastUpdate());
			user.setLoginProvider(userBean.getLoginProvider());
			user.setPassword(userBean.getPassword());
			user.setRole(userBean.getRole());
		}
		return user;
	}

	public static UserBean getUserBean(final User user) {
		UserBean userBean = null;
		if (user != null && user.getId() > 0) {
			userBean = new UserBean();
			userBean.setEmail(user.getEmail());
			userBean.setFirstName(user.getFirstName());
			userBean.setLastName(user.getLastName());
			userBean.setLastLogin(user.getLastLogin());
			userBean.setLastUpdate(user.getLastUpdate());
			userBean.setLoginProvider(user.getLoginProvider());
			userBean.setRole(user.getRole());
			userBean.setId(user.getId());
		}
		return userBean;
	}

	public static PostBean getPostBean(final Post post) {
		if (post == null) {
			return null;
		}
		final PostBean postBean = new PostBean();
		postBean.setCategory(post.getCategory());
		postBean.setImageUrls(post.getImageUrls());
		postBean.setPostCity(post.getPostCity());
		postBean.setPostCountry(post.getPostCountry());
		postBean.setPostState(post.getPostState());
		postBean.setPostText(post.getPostText());
		postBean.setPostTime(post.getPostTime());
		postBean.setPostUpdateTime(post.getPostUpdateTime());
		postBean.setPriority(post.getPriority());
		postBean.setPostid(post.getPostid());
		postBean.setPrice(post.getPrice());
		postBean.setPostTitle(post.getPostTitle());
		postBean.setUserEmail(post.getUserEmail());
		postBean.setUserName(post.getUserName());
		postBean.setPostContact(post.getPostContact());
		return postBean;
	}

	public static Post getPostEntity(final PostBean postBean) {
		if (postBean == null) {
			return null;
		}
		final Post post = new Post();
		post.setCategory(postBean.getCategory());
		post.setImageUrls(postBean.getImageUrls());
		post.setPostCity(postBean.getPostCity());
		post.setPostCountry(postBean.getPostCountry());
		post.setPostState(postBean.getPostState());
		post.setPostText(postBean.getPostText());
		post.setPostTime(new Date(System.currentTimeMillis()));
		post.setPostUpdateTime(new Date(System.currentTimeMillis()));
		post.setPriority(postBean.getPriority());
		post.setActive(ACTIVE);
		post.setPrice(postBean.getPrice());
		post.setPostTitle(postBean.getPostTitle());
		post.setUserEmail(postBean.getUserEmail());
		post.setUserName(postBean.getUserName());
		post.setPostContact(postBean.getPostContact());
		return post;
	}

	public static void getPostEntityForUpdate(final Post post, final PostBean postBean) {
		post.setPostTitle(postBean.getPostTitle());
		post.setCategory(postBean.getCategory());
		post.setImageUrls(postBean.getImageUrls());
		post.setPostText(postBean.getPostText());
		post.setPostUpdateTime(new Date(System.currentTimeMillis()));
		post.setPriority(postBean.getPriority());
	}

}
