/**
 *
 */
package com.hbajar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hbajar.beans.BaseBean;
import com.hbajar.beans.UserBean;
import com.hbajar.dao.UserDAO;

/**
 * @author SBhattarai
 *
 */
@RestController
public class AccessController {

	@Autowired
	private UserDAO userDao;

	@RequestMapping(method = { RequestMethod.POST }, value = { "/register" })
	@ResponseBody
	public BaseBean register(@RequestBody final UserBean userBean) throws Exception {
		return userDao.register(userBean);
	}

	@RequestMapping(method = { RequestMethod.POST }, value = { "/login" })
	@ResponseBody
	public BaseBean login(@RequestBody final UserBean userBean) throws Exception {
		return userDao.login(userBean);
	}

}
