/**
 * 
 */
package com.hbajar.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.annotation.MultipartConfig;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hbajar.beans.BaseBean;

/**
 * @author SBhattarai
 *
 */
@RestController
@MultipartConfig(fileSizeThreshold = 1048576)
public class ImageController {

	private static Logger LOG = Logger.getLogger(ImageController.class);

	@Autowired
	private Environment environment;

	@RequestMapping(value = "/upload", method = { RequestMethod.POST })
	@ResponseBody
	public BaseBean uploadImage(@RequestParam("uploadedFile") MultipartFile uploadedFileRef) {
		BaseBean result = new BaseBean();
		try {
			result.setStatus(saveImage(uploadedFileRef));

		} catch (IOException e) {
			LOG.error("Error on saving image", e);
		}
		return result;
	}

	@RequestMapping(value = "/getImage/{imageId}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] testImage3(@PathVariable("imageId") String imageId) throws IOException {
		File file = new File(environment.getProperty("image.location") + "//" + imageId + ".jpg");
		InputStream in = new FileInputStream(file);

		try {
			return IOUtils.toByteArray(in);
		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	private boolean saveImage(MultipartFile uploadedFileRef) throws IOException {
		// Get name of uploaded file.
		String fileName = uploadedFileRef.getOriginalFilename();

		// Path where the uploaded file will be stored.
		String path = environment.getProperty("image.location") + "//" + fileName;

		// This buffer will store the data read from 'uploadedFileRef'
		byte[] buffer = new byte[1000];

		// Now create the output file on the server.
		File outputFile = new File(path);

		FileInputStream reader = null;
		FileOutputStream writer = null;

		outputFile.createNewFile();

		// Create the input stream to uploaded file to read data from it.
		reader = (FileInputStream) uploadedFileRef.getInputStream();

		// Create writer for 'outputFile' to write data read from
		// 'uploadedFileRef'
		writer = new FileOutputStream(outputFile);

		// Iteratively read data from 'uploadedFileRef' and write to
		// 'outputFile';
		while (reader.read(buffer) != -1) {
			writer.write(buffer);
		}
		reader.close();
		writer.close();
		return true;
	}

}
