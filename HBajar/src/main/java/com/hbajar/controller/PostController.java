/**
 *
 */
package com.hbajar.controller;

import io.swagger.annotations.Api;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hbajar.beans.BaseBean;
import com.hbajar.beans.PostBean;
import com.hbajar.beans.ResultParamsBean;
import com.hbajar.dao.PostDAO;
import com.hbajar.util.LoggerUtil;

/**
 * @author SBhattarai
 *
 */
@RestController
@Api("Post Mangement API")
public class PostController {
	private static final Logger LOG = Logger.getLogger(PostController.class);
	@Autowired
	private PostDAO postDao;

	@RequestMapping(method = { RequestMethod.POST }, value = { "/post" })
	@ResponseBody
	public BaseBean addNewPost(@RequestBody final PostBean postBean) throws Exception {
		LOG.info("Adding A Post");
		LoggerUtil.log(postBean);
		return postDao.addNewPost(postBean);
	}

	@RequestMapping(method = { RequestMethod.POST }, value = { "/updatePost" })
	@ResponseBody
	public BaseBean updatePost(@RequestBody final PostBean postBean) throws Exception {
		LOG.info("Updating A Post");
		LoggerUtil.log(postBean);
		return postDao.updatePost(postBean);
	}

	@RequestMapping(method = { RequestMethod.GET }, value = { "{postId}/deletePost" })
	@ResponseBody
	public BaseBean deletePost(@PathVariable("postId") final Integer postId) throws Exception {
		LOG.info("Deleting Post postId: " + postId);
		return postDao.deletePost(postId);
	}

	@RequestMapping(method = { RequestMethod.POST }, value = { "/getMorePosts" })
	@ResponseBody
	public List<PostBean> getMorePosts(@RequestBody final ResultParamsBean resultParams) throws Exception {
		LOG.info("User is asking for posts with params: ");
		LoggerUtil.log(resultParams);
		return postDao.getPosts(resultParams);
	}

	@RequestMapping(method = { RequestMethod.GET }, value = { "/getMorePosts" })
	@ResponseBody
	public List<PostBean> getMorePostsGet(@RequestBody final ResultParamsBean resultParams) throws Exception {
		return postDao.getPosts(resultParams);
	}
}
