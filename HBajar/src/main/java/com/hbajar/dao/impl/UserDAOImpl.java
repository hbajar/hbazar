/**
 *
 */
package com.hbajar.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hbajar.beans.BaseBean;
import com.hbajar.beans.MessageStatus;
import com.hbajar.beans.PostBean;
import com.hbajar.beans.UserBean;
import com.hbajar.dao.UserDAO;
import com.hbajar.entities.Post;
import com.hbajar.entities.User;
import com.hbajar.util.DaoUtil;
import com.hbajar.util.TransformerUtil;

/**
 * @author SBhattarai
 *
 */
@Transactional
@Repository
public class UserDAOImpl implements UserDAO {

	@Value("${user.exists}")
	private String userExists;

	@Value("${user.registered}")
	private String userRegistered;

	@Value("${result.size}")
	private int resultSize;

	@Value("${result.days}")
	private int resultDays;

	@Autowired
	private SessionFactory factory;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hbajar.dao.UserDAO#register(com.hbajar.beans.UserBean)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public BaseBean register(final UserBean userBean) throws Exception {
		final User user = TransformerUtil.getUserEntity(userBean);
		final BaseBean bean = new BaseBean();
		if (user != null) {
			final Session session = factory.getCurrentSession();
			final List<User> savedUsers = session.createCriteria(User.class)
					.add(Restrictions.eq("email", userBean.getEmail())).list();
			if (savedUsers == null || savedUsers.size() == 0) {
				session.save(user);
				bean.setStatus(true);
				bean.setMessage(MessageStatus.REGISTER_SUCCESS);
			} else {
				bean.setStatus(false);
				bean.setMessage(MessageStatus.REGISTER_FAIL);
			}
		}
		return bean;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hbajar.dao.UserDAO#login(com.hbajar.beans.UserBean)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public UserBean login(final UserBean userBean) throws Exception {
		UserBean bean = new UserBean();
		if (userBean != null) {
			final Session session = factory.getCurrentSession();
			final List<User> users = session.createCriteria(User.class)
					.add(Restrictions.eq("email", userBean.getEmail()))
					.add(Restrictions.eq("password", userBean.getPassword())).list();
			if (users != null && users.size() == 1) {

				bean = TransformerUtil.getUserBean(users.get(0));

				final List<Post> posts = DaoUtil.getUserLocationSpecificPosts(userBean.getResultParams(), session,
						resultSize, resultDays);

				final List<PostBean> postBeans = new ArrayList<PostBean>();

				for (final Post post : posts) {
					final PostBean postBean = TransformerUtil.getPostBean(post);
					postBeans.add(postBean);
				}

				bean.setPosts(postBeans);

				bean.setStatus(true);
				bean.setMessage(MessageStatus.LOGIN_SUCCESS);

			} else {
				bean.setStatus(false);
				bean.setMessage(MessageStatus.LOGIN_ERROR);
			}
		}
		return bean;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hbajar.dao.UserDAO#updateUserInfo(com.hbajar.beans.UserBean)
	 */
	@Override
	public BaseBean updateUserInfo(final UserBean user) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
