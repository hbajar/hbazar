/**
 *
 */
package com.hbajar.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hbajar.beans.BaseBean;
import com.hbajar.beans.MessageStatus;
import com.hbajar.beans.PostBean;
import com.hbajar.beans.ResultParamsBean;
import com.hbajar.dao.PostDAO;
import com.hbajar.entities.Post;
import com.hbajar.util.DaoUtil;
import com.hbajar.util.TransformerUtil;

/**
 * @author SBhattarai
 *
 */
@Repository
@Transactional
public class PostDAOImpl implements PostDAO {

	private static final byte INACTIVE = 0;

	@Value("${result.size}")
	private int resultSize;

	@Value("${result.days}")
	private int resultDays;

	@Autowired
	private SessionFactory factory;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hbajar.dao.PostDAO#addNewPost(com.hbajar.beans.PostBean)
	 */
	@Override
	public BaseBean addNewPost(final PostBean postBean) throws Exception {
		final BaseBean result = new BaseBean();
		final Session session = factory.getCurrentSession();
		Post post = TransformerUtil.getPostEntity(postBean);
		session.save(post);
		result.setMessage(MessageStatus.POST_SAVED);
		result.setStatus(true);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hbajar.dao.PostDAO#updatePost(com.hbajar.beans.PostBean)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public BaseBean updatePost(final PostBean postBean) throws Exception {
		final BaseBean result = new BaseBean();
		final Session session = factory.getCurrentSession();
		final List<Post> posts = session.createCriteria(Post.class).add(Restrictions.eq("postid", postBean.getPostid())).list();
		if (posts != null && posts.size() == 1) {
			final Post post = posts.get(0);
			TransformerUtil.getPostEntityForUpdate(post, postBean);
			session.save(post);
			result.setMessage(MessageStatus.POST_SAVED);
			result.setStatus(true);
		} else {
			result.setMessage(MessageStatus.POST_SAVE_ERROR);
			result.setStatus(false);
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hbajar.dao.PostDAO#deletePost(int)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public BaseBean deletePost(final int postid) throws Exception {
		final BaseBean result = new BaseBean();
		final Session session = factory.getCurrentSession();
		final List<Post> posts = session.createCriteria(Post.class).add(Restrictions.eq("postid", postid)).list();
		if (posts != null && posts.size() == 1) {
			final Post post = posts.get(0);
			post.setActive(INACTIVE);
			session.delete(post);
			result.setMessage(MessageStatus.POST_SAVED);
			result.setStatus(true);
		} else {
			result.setMessage(MessageStatus.POST_SAVE_ERROR);
			result.setStatus(false);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hbajar.dao.PostDAO#getPosts(com.hbajar.beans.ResultParamsBean)
	 */
	@Override
	public List<PostBean> getPosts(final ResultParamsBean params) throws Exception {
		final List<PostBean> postBeans = new ArrayList<PostBean>();
		final Session session = factory.getCurrentSession();
		final List<Post> posts = DaoUtil.getUserLocationSpecificPosts(params, session, resultSize, resultDays);
		for (final Post post : posts) {
			postBeans.add(TransformerUtil.getPostBean(post));
		}
		return postBeans;
	}

}
