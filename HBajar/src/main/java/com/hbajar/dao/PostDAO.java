/**
 *
 */
package com.hbajar.dao;

import java.util.List;

import com.hbajar.beans.BaseBean;
import com.hbajar.beans.PostBean;
import com.hbajar.beans.ResultParamsBean;

/**
 * @author SBhattarai
 *
 */
public interface PostDAO {

	/**
	 * Add new post
	 *
	 * @param post
	 * @return
	 * @throws Exception
	 */
	public BaseBean addNewPost(PostBean post) throws Exception;

	/**
	 * update Existing post
	 *
	 * @param post
	 * @return
	 * @throws Exception
	 */
	public BaseBean updatePost(PostBean post) throws Exception;

	/**
	 * delete Existing post
	 *
	 * @param post
	 * @return
	 * @throws Exception
	 */
	public BaseBean deletePost(int postid) throws Exception;

	/**
	 * Get the next set of posts
	 * 
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List<PostBean> getPosts(ResultParamsBean params) throws Exception;

}
