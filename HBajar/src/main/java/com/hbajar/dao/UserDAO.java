/**
 *
 */
package com.hbajar.dao;

import com.hbajar.beans.BaseBean;
import com.hbajar.beans.UserBean;

/**
 * @author SBhattarai
 *
 */
public interface UserDAO {

	public BaseBean register(UserBean user) throws Exception;

	public UserBean login(UserBean user) throws Exception;

	public BaseBean updateUserInfo(UserBean user) throws Exception;

}
