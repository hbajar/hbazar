/**
 *
 */
package com.hbajar.beans;

import java.io.Serializable;

/**
 * @author SBhattarai
 *
 */
public class ResultParamsBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String postCity;
	private String postState;
	private String postCountry;
	private int rowNum;
	private int category;

	/**
	 * @return the category
	 */
	public int getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(final int category) {
		this.category = category;
	}

	/**
	 * @return the rowNum
	 */
	public int getRowNum() {
		return rowNum;
	}

	/**
	 * @param rowNum
	 *            the rowNum to set
	 */
	public void setRowNum(final int rowNum) {
		this.rowNum = rowNum;
	}

	/**
	 * @return the postCity
	 */
	public String getPostCity() {
		return postCity;
	}

	/**
	 * @param postCity
	 *            the postCity to set
	 */
	public void setPostCity(final String postCity) {
		this.postCity = postCity;
	}

	/**
	 * @return the postState
	 */
	public String getPostState() {
		return postState;
	}

	/**
	 * @param postState
	 *            the postState to set
	 */
	public void setPostState(final String postState) {
		this.postState = postState;
	}

	/**
	 * @return the postCountry
	 */
	public String getPostCountry() {
		return postCountry;
	}

	/**
	 * @param postCountry
	 *            the postCountry to set
	 */
	public void setPostCountry(final String postCountry) {
		this.postCountry = postCountry;
	}

}
