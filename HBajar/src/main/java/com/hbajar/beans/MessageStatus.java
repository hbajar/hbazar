/**
 * 
 */
package com.hbajar.beans;

/**
 * @author SBhattarai
 *
 */
public enum MessageStatus {
	
	LOGIN_ERROR(0), 
	LOGIN_SUCCESS(1),
	POST_SAVED(2),
	POST_SAVE_ERROR(3),
	REGISTER_SUCCESS(4),
	REGISTER_FAIL(5),
	POST_DELETED(6),
	POST_DELETE_ERROR(7);
	
	private int value;
	
	private MessageStatus(int value){
		this.value = value;
	}

}
