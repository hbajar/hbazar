/**
 *
 */
package com.hbajar.beans;

import java.io.Serializable;

/**
 * @author SBhattarai
 *
 */
public class BaseBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean status;
	private MessageStatus message;

	/**
	 * @return the status
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(final boolean status) {
		this.status = status;
	}

	/**
	 * @return the message
	 */
	public MessageStatus getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(final MessageStatus message) {
		this.message = message;
	}

}
