package com.hbajar.beans;

// Generated Sep 29, 2015 10:17:33 AM by Hibernate Tools 4.0.0

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserBean extends BaseBean implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String email;
	private String firstName;
	private String lastName;
	private String password;
	private String role;
	private Integer loginProvider;
	private Date lastLogin;
	private Date lastUpdate;
	private String userAccountcol;
	private List<PostBean> posts = new ArrayList<PostBean>();
	private ResultParamsBean resultParams;

	/**
	 * @return the resultParams
	 */
	public ResultParamsBean getResultParams() {
		return resultParams;
	}

	/**
	 * @param resultParams
	 *            the resultParams to set
	 */
	public void setResultParams(final ResultParamsBean resultParams) {
		this.resultParams = resultParams;
	}

	/**
	 * @return the posts
	 */
	public List<PostBean> getPosts() {
		return posts;
	}

	/**
	 * @param posts
	 *            the posts to set
	 */
	public void setPosts(final List<PostBean> posts) {
		this.posts = posts;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(final String password) {
		this.password = password;
	}

	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role
	 *            the role to set
	 */
	public void setRole(final String role) {
		this.role = role;
	}

	/**
	 * @return the loginProvider
	 */
	public Integer getLoginProvider() {
		return loginProvider;
	}

	/**
	 * @param loginProvider
	 *            the loginProvider to set
	 */
	public void setLoginProvider(final Integer loginProvider) {
		this.loginProvider = loginProvider;
	}

	/**
	 * @return the lastLogin
	 */
	public Date getLastLogin() {
		return lastLogin;
	}

	/**
	 * @param lastLogin
	 *            the lastLogin to set
	 */
	public void setLastLogin(final Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	/**
	 * @return the lastUpdate
	 */
	public Date getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * @param lastUpdate
	 *            the lastUpdate to set
	 */
	public void setLastUpdate(final Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	/**
	 * @return the userAccountcol
	 */
	public String getUserAccountcol() {
		return userAccountcol;
	}

	/**
	 * @param userAccountcol
	 *            the userAccountcol to set
	 */
	public void setUserAccountcol(final String userAccountcol) {
		this.userAccountcol = userAccountcol;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
