package com.hbajar.beans;

import java.io.Serializable;
import java.util.Date;

public class PostBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private int postid;
	private String postText;
	private String imageUrls;
	private int category;
	private int priority;
	private Date postTime;
	private Date postUpdateTime;
	private String postCity;
	private String postState;
	private String postCountry;
	private byte active;
	private double price;
	private String postTitle;
	private String userName;
	private String userEmail;
	private String postContact;

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the userEmail
	 */
	public String getUserEmail() {
		return userEmail;
	}

	/**
	 * @param userEmail
	 *            the userEmail to set
	 */
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	/**
	 * @return the postContact
	 */
	public String getPostContact() {
		return postContact;
	}

	/**
	 * @param postContact
	 *            the postContact to set
	 */
	public void setPostContact(String postContact) {
		this.postContact = postContact;
	}

	/**
	 * @return the postTitle
	 */
	public String getPostTitle() {
		return postTitle;
	}

	/**
	 * @param postTitle
	 *            the postTitle to set
	 */
	public void setPostTitle(final String postTitle) {
		this.postTitle = postTitle;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(final double price) {
		this.price = price;
	}

	/**
	 * @return the postCity
	 */
	public String getPostCity() {
		return postCity;
	}

	/**
	 * @param postCity
	 *            the postCity to set
	 */
	public void setPostCity(final String postCity) {
		this.postCity = postCity;
	}

	/**
	 * @return the postState
	 */
	public String getPostState() {
		return postState;
	}

	/**
	 * @param postState
	 *            the postState to set
	 */
	public void setPostState(final String postState) {
		this.postState = postState;
	}

	/**
	 * @return the postCountry
	 */
	public String getPostCountry() {
		return postCountry;
	}

	/**
	 * @param postCountry
	 *            the postCountry to set
	 */
	public void setPostCountry(final String postCountry) {
		this.postCountry = postCountry;
	}

	/**
	 * @return the postid
	 */
	public int getPostid() {
		return postid;
	}

	/**
	 * @param postid
	 *            the postid to set
	 */
	public void setPostid(final int postid) {
		this.postid = postid;
	}

	/**
	 * @return the postText
	 */
	public String getPostText() {
		return postText;
	}

	/**
	 * @param postText
	 *            the postText to set
	 */
	public void setPostText(final String postText) {
		this.postText = postText;
	}

	/**
	 * @return the imageUrls
	 */
	public String getImageUrls() {
		return imageUrls;
	}

	/**
	 * @param imageUrls
	 *            the imageUrls to set
	 */
	public void setImageUrls(final String imageUrls) {
		this.imageUrls = imageUrls;
	}

	/**
	 * @return the category
	 */
	public int getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(final int category) {
		this.category = category;
	}

	/**
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}

	/**
	 * @param priority
	 *            the priority to set
	 */
	public void setPriority(final int priority) {
		this.priority = priority;
	}

	/**
	 * @return the postTime
	 */
	public Date getPostTime() {
		return postTime;
	}

	/**
	 * @param postTime
	 *            the postTime to set
	 */
	public void setPostTime(final Date postTime) {
		this.postTime = postTime;
	}

	/**
	 * @return the postUpdateTime
	 */
	public Date getPostUpdateTime() {
		return postUpdateTime;
	}

	/**
	 * @param postUpdateTime
	 *            the postUpdateTime to set
	 */
	public void setPostUpdateTime(final Date postUpdateTime) {
		this.postUpdateTime = postUpdateTime;
	}

	/**
	 * @return the active
	 */
	public byte getActive() {
		return active;
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(final byte active) {
		this.active = active;
	}

}
